﻿using System;

namespace openxml_test {

   class Program {

      static void Main(string[] args) {
         if (args.Length < 1) {
            Console.WriteLine("Usage: dotnet run -- <FILENAME>");
         }
         else {
            try {
               var test = new Test();
               test.Run(args[0]);
            }
            catch (Exception exc) {
               Console.WriteLine($"Could not do what I was supposed to do: {exc.Message}");
            }
         }
         Console.WriteLine("Press enter to continue...");
         Console.ReadLine();
      }
   }
}
