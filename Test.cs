using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;

class Test {

   public void Run(string filePath) {
      using (var doc = WordprocessingDocument.Open(filePath, true)) {
         var body = doc.MainDocumentPart.Document.Body;
         var paragraphs = body.Descendants<Paragraph>();
         var paragraphTexts = from par in paragraphs
                              let texts = par.Descendants<Text>().Where(t => !t.Ancestors().Any(a => a is TextBoxContent)).Select(t => t.Text)
                              select string.Join("", texts);
         var text = string.Join(Environment.NewLine, paragraphTexts);
         Console.WriteLine($"Document text:\n{text}");

         // Print bookmark names
         var bookmarkStarts = body.Descendants<BookmarkStart>();
         var bookmarkEnds = body.Descendants<BookmarkEnd>();
         var bookmarkNames = bookmarkStarts.Select(b => $"\"{b.Name}\"");
         Console.WriteLine($"\n\nBookmarks found in main document part: {string.Join(", ", bookmarkNames)}\n");

         // Get all merge fields
         var fields = body.GetFields();
         
         // Replace field texts
         foreach(var field in fields) {
            field.Text = "ERSETZT";
            field.Remove();
         }

         // Save document
         string fileName = Path.GetFileNameWithoutExtension(filePath);
         string dir = Path.GetDirectoryName(filePath);
         string filePathNew = $"{dir}\\{fileName}_REPLACED.docx";
         Console.WriteLine($"Saving modified document as ${filePathNew}.");
         doc.SaveAs(filePathNew);
      }
   }
}

internal static class Extensions {

   public static IEnumerable<FieldInfo> GetFields(this OpenXmlElement elem) {
      var fieldCodes = elem.Descendants<FieldCode>();
      foreach (var fldCode in fieldCodes) {
         Run rFldCode = (Run) fldCode.Parent;
         // Get the three other Runs that make up the merge field
         Run rBegin = rFldCode.PreviousSibling<Run>();
         Run rSep = rFldCode.NextSibling<Run>();
         Run rText = rSep.NextSibling<Run>();
         Run rEnd = rText.NextSibling<Run>();
         yield return new FieldInfo(rBegin, rFldCode, rSep, rText, rEnd);
      }
   }

}
