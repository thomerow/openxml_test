﻿using System;
using DocumentFormat.OpenXml.Wordprocessing;

public class FieldInfo {
   private Run m_rBegin;
   private Run m_rFldCode;
   private Run m_rSep;
   private Run m_rText;
   private Run m_rEnd;
   private bool m_bRemoved = false;

   public FieldInfo(Run rBegin, Run rFldCode, Run rSep, Run rText, Run rEnd) {
      this.m_rBegin = rBegin;
      this.m_rFldCode = rFldCode;
      this.m_rSep = rSep;
      this.m_rText = rText;
      this.m_rEnd = rEnd;
   }

   public Run RBegin { get => m_rBegin; set => m_rBegin = value; }
   public Run RFldCode { get => m_rFldCode; set => m_rFldCode = value; }
   public Run RSep { get => m_rSep; set => m_rSep = value; }
   public Run RText { get => m_rText; set => m_rText = value; }
   public Run REnd { get => m_rEnd; set => m_rEnd = value; }

   public string Text {
      get {
         Text t = m_rText.GetFirstChild<Text>();
         return t.Text;
      }
      set {
         Text t = m_rText.GetFirstChild<Text>();
         t.Text = value;
      }
   }

   internal void Remove() {
      if (m_bRemoved) return;
      m_rFldCode.Remove();
      m_rBegin.Remove();
      m_rSep.Remove();
      m_rEnd.Remove();
      m_bRemoved = true;
   }
}